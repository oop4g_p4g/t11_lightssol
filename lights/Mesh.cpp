#include "Mesh.h"
#include "D3DUtil.h"
#include "FX.h"
#include "D3D.h"
#include "WindowUtils.h"

void SubMesh::Release()
{
	ReleaseCOM(mpVB);
	ReleaseCOM(mpIB);
	mNumIndices = mNumVerts = 0;
}

Mesh& MeshMgr::CreateMesh(const std::string& name)
{
	for (int i = 0; i < (int)mMeshes.size(); ++i)
		if (mMeshes[i]->mName == name)
			assert(false);
	mMeshes.push_back(new Mesh(name));
	return *(mMeshes.back());
}

Mesh* MeshMgr::GetMesh(const std::string& name)
{
	if (mMeshes.empty())
		return nullptr;
	size_t i = 0;
	while (mMeshes[i]->mName != name) ++i;
	if (i >= mMeshes.size())
		return nullptr;
	return mMeshes[i];
}


void MeshMgr::Release()
{
	for (int i = 0; i < (int)mMeshes.size(); ++i)
		delete mMeshes[i];
	mMeshes.clear();
}

void Mesh::Release()
{
	for (int i = 0; i < (int)mSubMeshes.size(); ++i)
		delete mSubMeshes[i];
	mSubMeshes.clear();
}



void Mesh::CreateFrom(const VertexPosNorm verts[], int numVerts, const unsigned int indices[], int numIndices, 
	const Material& mat, int meshStartIndex, int meshNumIndices)
{
	Release();
	SubMesh*p = new SubMesh;
	mSubMeshes.push_back(p);
	p->mNumIndices = meshNumIndices;
	p->mNumVerts = numVerts;
	p->material = mat;
	MyD3D& d3d = WinUtil::Get().GetD3D();
	CreateVertexBuffer(d3d.GetDevice(), sizeof(VertexPosNorm)*numVerts, verts, p->mpVB);
	CreateIndexBuffer(d3d.GetDevice(), sizeof(unsigned int)*numIndices, indices, p->mpIB);

}
