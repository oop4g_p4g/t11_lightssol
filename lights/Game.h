#ifndef GAME_H
#define GAME_H

#include <vector>

#include "Mesh.h"
#include "Model.h"
#include "Singleton.h"

class MyD3D;

class Game : public Singleton<Game>
{
public:
	~Game() {
		Release();
	}
	void Update(float dTime);
	void Render(float dTime);
	void Initialise();
	void Release();
	LRESULT WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	const DirectX::SimpleMath::Vector3 mDefCamPos = DirectX::SimpleMath::Vector3(0, 2, -5);
	DirectX::SimpleMath::Vector3 mCamPos = DirectX::SimpleMath::Vector3(0, 2, -5);
	Model mBox, mQuad, mLight;
	std::vector<Model> mTrees;

private:

	void BuildCube();
	void BuildQuad();
	void BuildPyramid();

	float gAngle = 0;
};


#endif

