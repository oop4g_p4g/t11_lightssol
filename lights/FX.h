#ifndef FX_H
#define FX_H

#include <string>
#include <d3d11.h>
#include "ShaderTypes.h"
#include "D3DUtil.h"

class Model;
class MyD3D;

namespace FX
{
	DirectX::SimpleMath::Matrix& GetProjectionMatrix();
	DirectX::SimpleMath::Matrix& GetViewMatrix();

	//we've loaded in a "blob" of compiled shader code, it needs to be set up on the gpu as a pixel shader
	void CreatePixelShader(ID3D11Device& d3dDevice, char* pBuff, unsigned int buffSz, ID3D11PixelShader* &pPS);
	//we've loaded in a "blob" of compiled shader code, it needs to be set up on the gpu as a vertex shader
	void CreateVertexShader(ID3D11Device& d3dDevice, char* pBuff, unsigned int buffSz, ID3D11VertexShader* &pVS);
	//the input assembler needs to know what is in the vertex buffer, how to get the data out and which vertex shader to give it to
	void CreateInputLayout(ID3D11Device& d3dDevice, const D3D11_INPUT_ELEMENT_DESC vdesc[], int numElements, char* pBuff, unsigned int buffSz, ID3D11InputLayout** pLayout);

	void CreateConstantBuffer(ID3D11Device& d3dDevice, UINT sizeOfBuffer, ID3D11Buffer **pBuffer);
	void CheckShaderModel5Supported(ID3D11Device& d3dDevice);
	//load in and allocate a chunk of binary data
	char* ReadAndAllocate(const std::string& fileName, unsigned int& bytesRead);


	void CreateConstantBuffers(MyD3D& d3d);
	void ReleaseConstantBuffers();
	void SetPerObjConsts(ID3D11DeviceContext& ctx, DirectX::SimpleMath::Matrix &world);
	void SetPerFrameConsts(ID3D11DeviceContext& ctx, const DirectX::SimpleMath::Vector3& eyePos);
	void PreRenderObj(ID3D11DeviceContext& ctx, Material& mat);
	void SetupDirectionalLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &direction,
		const DirectX::SimpleMath::Vector3& diffuse = DirectX::SimpleMath::Vector3(1, 1, 1),
		const DirectX::SimpleMath::Vector3& specular = DirectX::SimpleMath::Vector3(0, 0, 0),
		const DirectX::SimpleMath::Vector3& ambient = DirectX::SimpleMath::Vector3(0, 0, 0));

	void SetupPointLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &position,
		const DirectX::SimpleMath::Vector3& diffuse = DirectX::SimpleMath::Vector3(1, 1, 1),
		const DirectX::SimpleMath::Vector3& specular = DirectX::SimpleMath::Vector3(0, 0, 0),
		const DirectX::SimpleMath::Vector3& ambient = DirectX::SimpleMath::Vector3(0, 0, 0),
		float range = 1000.f, float atten1 = 0.05f);

	void SetupSpotLight(int lightIdx, bool enable,
		const DirectX::SimpleMath::Vector3 &position,
		const DirectX::SimpleMath::Vector3 &direction,
		const DirectX::SimpleMath::Vector3& diffuse = DirectX::SimpleMath::Vector3(1, 1, 1),
		const DirectX::SimpleMath::Vector3& specular = DirectX::SimpleMath::Vector3(0, 0, 0),
		const DirectX::SimpleMath::Vector3& ambient = DirectX::SimpleMath::Vector3(0, 0, 0),
		float range = 1000.f, float atten1 = 0.05f, float innerConeTheta = D2R(30), float outerConePhi = D2R(40));

	typedef enum { NONE = 0, LIT = 1 } FxFlags;


	class MyFX
	{
	public:
		MyFX(MyD3D& d3d)
			:mD3D(d3d) {}
		~MyFX() {
			Release();
		}
		bool Init();
		void Release();
		void Render(Model& model, unsigned int flags, Material* pOverrideMat = nullptr);


	private:
		ID3D11InputLayout* mpInputLayout = nullptr;
		ID3D11VertexShader* mpVS = nullptr;
		ID3D11PixelShader* mpPSLit = nullptr, *mpPSUnlit = nullptr;
		MyD3D& mD3D;
	};

}

#endif
