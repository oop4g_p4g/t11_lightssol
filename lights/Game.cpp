#include "D3D.h"
#include "SimpleMath.h"
#include "d3dutil.h"
#include "Game.h"
#include "WindowUtils.h"
#include "FX.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;


void Game::BuildQuad()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//quad in the XZ plane
		{ Vector3(-1, 0, -1), Vector3(0, 1, 0) },
		{ Vector3(-1, 0, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 0, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 1, 0) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3
	};
	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("quad");
	Material mat(Vector4(1, 1, 1, 0), Vector4(0, 1, 0, 0), Vector4(1, 1, 1, 10));
	mesh.CreateFrom(vertices, 4, indices, 6, mat, 0, 6);
	mQuad.Initialise(mesh);
}

void Game::BuildPyramid()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//front
		{ Vector3(-1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 1, 0), Vector3(0, 0, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 0, 0) },
		//left
		{ Vector3(-1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 0, 1), Vector3(0, 0, 0) },
		{ Vector3(0, 1, 0), Vector3(0, 0, 0) },
		//right
		{ Vector3(1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 1, 0), Vector3(0, 0, 0) },
		{ Vector3(0, 0, 1), Vector3(0, 0, 0) },
		//bottom
		{ Vector3(-1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(1, 0, -1), Vector3(0, 0, 0) },
		{ Vector3(0, 0, 1), Vector3(0, 0, 0) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		3, 4, 5,
		6, 7, 8,
		9, 10, 11
	};

	for (int i = 0; i < 4; ++i)
	{
		int idx = i * 3;
		Vector3 a(vertices[idx].Pos - vertices[idx + 1].Pos), b(vertices[idx + 2].Pos - vertices[idx + 1].Pos);
		a.Normalize();
		b.Normalize();
		vertices[idx].Norm = vertices[idx + 1].Norm = vertices[idx + 2].Norm = b.Cross(a);
	}

	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("pyramid");
	Material mat(Vector4(0.1f, 0.1f, 0.1f, 0), Vector4(0.2f, 1, 1, 0), Vector4(0.2f, 0.2f, 0.2f, 1));
	mesh.CreateFrom(vertices, 12, indices, 12, mat, 0, 12);
	mTrees.clear();
	mTrees.insert(mTrees.begin(), 100, Model());
	for (int i = 0; i < (int)mTrees.size(); ++i)
		mTrees[i].Initialise(mesh);
	int w = (int)sqrt(mTrees.size());
	for (int y = 0; y < w; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			Model& d = mTrees[y * w + x];
			float xo = -2 + (float)x*0.5f;
			float zo = -2 + (float)y*0.5f;
			d.GetPosition() = Vector3(xo + GetRandom(-0.1f, 0.1f), -1, zo + GetRandom(-0.1f, 0.1f));
			d.GetScale() = Vector3(GetRandom(0.1f, 0.25f), GetRandom(0.6f, 1.f), GetRandom(0.1f, 0.25f));
			d.GetRotation() = Vector3(0, GetRandom(0.f, 2 * PI), 0);
			Material mat = { Vector4(1,1,1, 0), Vector4(GetRandom(0.f, 0.4f), GetRandom(0.6f, 1.f), GetRandom(0.f, 0.3f), 0), Vector4(0.4f, 0.4f, 0.4f, 10) };
			d.SetOverrideMat(&mat);
		}
	}
}

void Game::BuildCube()
{
	// Create vertex buffer
	VertexPosNorm vertices[] =
	{	//front
		{ Vector3(-1, -1, -1), Vector3(0, 0, -1) },
		{ Vector3(-1, 1, -1), Vector3(0, 0, -1) },
		{ Vector3(1, 1, -1), Vector3(0, 0, -1) },
		{ Vector3(1, -1, -1), Vector3(0, 0, -1) },
		//right
		{ Vector3(1, -1, -1), Vector3(1, 0, 0) },
		{ Vector3(1, 1, -1), Vector3(1, 0, 0) },
		{ Vector3(1, 1, 1), Vector3(1, 0, 0) },
		{ Vector3(1, -1, 1), Vector3(1, 0, 0) },
		//left
		{ Vector3(-1, -1, 1), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, 1), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, -1), Vector3(-1, 0, 0) },
		{ Vector3(-1, -1, -1), Vector3(-1, 0, 0) },
		//top
		{ Vector3(-1, 1, -1), Vector3(0, 1, 0) },
		{ Vector3(-1, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, -1), Vector3(0, 1, 0) },
		//bottom
		{ Vector3(1, -1, -1), Vector3(0, -1, 0) },
		{ Vector3(1, -1, 1), Vector3(0, -1, 0) },
		{ Vector3(-1, -1, 1), Vector3(0, -1, 0) },
		{ Vector3(-1, -1, -1), Vector3(0, -1, 0) },
		//back
		{ Vector3(1, -1, 1), Vector3(0, 0, 1) },
		{ Vector3(1, 1, 1), Vector3(0, 0, 1) },
		{ Vector3(-1, 1, 1), Vector3(0, 0, 1) },
		{ Vector3(-1, -1, 1), Vector3(0, 0, 1) }
	};

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3,
		//right
		4, 5, 6,
		4, 6, 7,
		//left
		8, 9, 10,
		8, 10, 11,
		//top
		12, 13, 14,
		12, 14, 15,
		//bottom
		16, 17, 18,
		16, 18, 19,
		//back
		20, 21, 22,
		20, 22, 23
	};

	Mesh &mesh = WinUtil::Get().GetD3D().GetMeshMgr().CreateMesh("box");
	Material mat(Vector4(1, 1, 1, 0), Vector4(1, 0, 0, 0), Vector4(1, 1, 1, 10));
	mesh.CreateFrom(vertices, 24, indices, 36, mat, 0, 36);
	mBox.Initialise(mesh);
	mLight = mBox;
}


void Game::Initialise()
{
	BuildQuad();
	BuildCube();
	BuildPyramid();

	mBox.GetPosition() = Vector3(0, -0.75f, 0);
	mBox.GetScale() = Vector3(0.2f, 0.1f, 0.1f);

	mLight.GetScale() = Vector3(0.025f);

	mQuad.GetScale() = Vector3(3, 1, 3);
	mQuad.GetPosition() = Vector3(0, -1, 0);

	FX::SetupDirectionalLight(0, true, Vector3(0.7f, -0.7f, 0.7f), Vector3(0.07f, 0.05f, 0.05f), Vector3(1, 1, 1), Vector3(0.07f, 0.05f, 0.05f));
}

void Game::Release()
{
}

void Game::Update(float dTime)
{
	gAngle += dTime * 0.5f;
	mBox.GetRotation().y = gAngle;

	Matrix rot = Matrix::CreateRotationY(mBox.GetRotation().y);
	Vector3 dir = { 1, 0, 0 };
	dir = dir.TransformNormal(dir, rot);
	Vector3 colour = { 1,1,1 };
	FX::SetupSpotLight(2, true, mBox.GetPosition(), dir, colour, Vector3(0, 0, 0), Vector3(0, 0, 0), 4, 0.1f, D2R(1), D2R(20));
	FX::SetupPointLight(1, true, mBox.GetPosition(), colour, Vector3(0, 0, 0), Vector3(0, 0, 0), 100, 3.f);

}

void Game::Render(float dTime)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);

	float alpha = 0.5f + sinf(gAngle * 2)*0.5f;

	//point light
	mLight.GetPosition() = Vector3(-2 + alpha * 4, -0.8f, -2);
	Vector3 plightColour = Vector3(alpha, 0.7f, 0.7f);
	FX::SetupPointLight(3, true, mLight.GetPosition(), plightColour, Vector3(0.1f, 0.1f, 0.1f), Vector3(0, 0, 0), 1, 9.f);
	mLight.GetMesh().GetSubMesh(0).material.Diffuse = Vec3To4(plightColour, 0);
	d3d.GetFX().Render(mLight, FX::FxFlags::NONE);


	FX::SetPerFrameConsts(d3d.GetDeviceCtx(), mCamPos);

	CreateViewMatrix(FX::GetViewMatrix(), mCamPos, Vector3(0, 0, 0), Vector3(0, 1, 0));
	CreateProjectionMatrix(FX::GetProjectionMatrix(), 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	Matrix w = Matrix::CreateRotationY(sinf(gAngle));
	FX::SetPerObjConsts(d3d.GetDeviceCtx(), w);


	Material mat = { { 1, 1, 1, 0 }, { 1, 1, 1, 0 }, { 0, 0, 0, 1 } };
	//main cube
	d3d.GetFX().Render(mBox, FX::FxFlags::NONE, &mat);


	//floor
	mat.Diffuse = Vector4(0.4f, 1, 0.4f, 0);
	d3d.GetFX().Render(mQuad, FX::FxFlags::LIT, &mat);

	for (int i = 0; i < (int)mTrees.size(); ++i)
		d3d.GetFX().Render(mTrees[i], FX::FxFlags::LIT);

	d3d.EndRender();

}

LRESULT Game::WindowsMssgHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	}
	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

