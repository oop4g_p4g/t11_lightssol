#ifndef SHADERTYPES_H
#define SHADERTYPES_H

#include <d3d11.h>

#include "SimpleMath.h"

/*
This is what our vertex data will look like
*/
struct VertexPosNorm
{
	DirectX::SimpleMath::Vector3 Pos;
	DirectX::SimpleMath::Vector3 Norm;

	static const D3D11_INPUT_ELEMENT_DESC sVertexDesc[2];
};

/*
Insted of a colour in each vertex we define a material
for a group of primitves (an entire surface)
*/
struct Material
{
	Material(const DirectX::SimpleMath::Vector4& a, const DirectX::SimpleMath::Vector4& d, const DirectX::SimpleMath::Vector4& s) {
		Ambient = a;
		Diffuse = d;
		Specular = s;
	}
	Material() : Ambient(1, 1, 1, 1), Diffuse(1, 1, 1, 1), Specular(1, 1, 1, 1) {}
	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular; // w = SpecPower
};

struct Light
{
	Light() { type = OFF; }

	DirectX::SimpleMath::Vector4 Ambient;
	DirectX::SimpleMath::Vector4 Diffuse;
	DirectX::SimpleMath::Vector4 Specular;		//w=power
	DirectX::SimpleMath::Vector4 Direction;		//w=nothing
	DirectX::SimpleMath::Vector4 Position;		//w=nothing
	DirectX::SimpleMath::Vector4 Attenuation;	//w=range

	typedef enum {
		OFF = 0,		//ignore this light
		DIR = 1,	//directional
		POINT = 2,	//point light
		SPOT = 3	//spot light
	} Type;
	int type;
	float range;
	float theta;
	float phi;
};

//shader variables that don't change within one frame
const int MAX_LIGHTS = 8;
struct GfxParamsPerFrame
{
	Light lights[MAX_LIGHTS];
	DirectX::SimpleMath::Vector4 eyePosW;
};
static_assert((sizeof(GfxParamsPerFrame) % 16) == 0, "CB size not padded correctly");



//shader variables that don't change within one object
struct GfxParamsPerObj
{
	DirectX::SimpleMath::Matrix world;
	DirectX::SimpleMath::Matrix worldInvT;
	DirectX::SimpleMath::Matrix worldViewProj;
};
static_assert((sizeof(GfxParamsPerObj) % 16) == 0, "CB size not padded correctly");


//shader variables that don't change within an object's sub-mesh
struct GfxParamsPerMesh
{
	Material material;
};
static_assert((sizeof(GfxParamsPerMesh) % 16) == 0, "CB size not padded correctly");

#endif
